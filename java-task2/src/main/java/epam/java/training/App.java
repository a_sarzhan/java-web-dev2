package epam.java.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Epam - Java Web Development Training
 * Assel Sarzhanova
 * Task 2
 */

public class App {

    public static void main( String[] args ) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String userNumbers;
        do{
            System.out.println("Enter numbers (use space to split numbers):");
        }while((userNumbers = bufferedReader.readLine()).trim().equals(""));

        String[] arrayOfStringNumbers = userNumbers.split("\\s+");
        int[] arrayOfIntegerNumbers = new int[arrayOfStringNumbers.length];
        boolean isValidNumbers = false;

        while(!isValidNumbers) {
            try {
                arrayOfStringNumbers = userNumbers.split("\\s+");
                arrayOfIntegerNumbers = new int[arrayOfStringNumbers.length];
                for (int i = 0; i < arrayOfIntegerNumbers.length; i++) {
                    arrayOfIntegerNumbers[i] = Integer.parseInt(arrayOfStringNumbers[i]);
                }
                isValidNumbers = true;
            } catch (NumberFormatException ignore) {
                System.out.println("Found not valid numbers");
                System.out.println("Enter numbers again:");
                userNumbers = bufferedReader.readLine();
            }
        }

        bufferedReader.close();

        printNumberWithMinimumLength(arrayOfIntegerNumbers);

        printNumberWithMaximumLength(arrayOfIntegerNumbers);

        printNumberWithMinimumDifferentDigits(arrayOfIntegerNumbers);

        printNumberWithUnrepeatedDigits(arrayOfIntegerNumbers);

        printNumberWithIncreasingDigits(arrayOfIntegerNumbers);

    }

    public static void printNumberWithMinimumLength(int[] userNumbers){
        int numberWithMinimumLength = userNumbers[0];
        int minimumLength = 0;

        for (int i = 0; i < userNumbers.length; i++) {
            int number = userNumbers[i];
            int numberLength = 0;
            while(number > 0){
                number /= 10;
                numberLength++;
            }
            if((i == 0) || (i != 0 && numberLength < minimumLength)){
                minimumLength = numberLength;
                numberWithMinimumLength = userNumbers[i];
            }
        }

        System.out.printf("Number with shortest length = %d, [length = %d] \n", numberWithMinimumLength, minimumLength);
    }

    public static void printNumberWithMaximumLength(int[] userNumbers){
        int numberWithMaximumLength = userNumbers[0];
        int maximumLength = 0;

        for (int i = 0; i < userNumbers.length; i++) {
            int number = userNumbers[i];
            int numberLength = 0;
            while(number > 0){
                number /= 10;
                numberLength++;
            }
            if((i == 0) || (i != 0 && numberLength > maximumLength)){
                maximumLength = numberLength;
                numberWithMaximumLength = userNumbers[i];
            }
        }
        System.out.printf("Number with longest length = %d, [length = %d] \n", numberWithMaximumLength, maximumLength);
    }

    public static void printNumberWithMinimumDifferentDigits(int[] userNumbers){
        Set<Integer> uniqueDigitsSet = new HashSet<>();
        int differentDigitsCount = 0;
        int numberWithMinimumDifferentDigits = 0;

        for (int i = 0; i < userNumbers.length; i++) {
            int currentNumber = userNumbers[i];
            while(currentNumber > 0){
                uniqueDigitsSet.add(currentNumber % 10);
                currentNumber /= 10;
            }

            if(i == 0 || (uniqueDigitsSet.size() < differentDigitsCount)){
                differentDigitsCount = uniqueDigitsSet.size();
                numberWithMinimumDifferentDigits = userNumbers[i];
            }
            uniqueDigitsSet.clear();
        }
        System.out.println("Number with minimum different digits = " + numberWithMinimumDifferentDigits);
    }

    public static void printNumberWithIncreasingDigits(int[] userNumbers){
        for (int userNumber : userNumbers) {
            int nextDigit = 0;
            int digit = 0;
            int validityCounter = 0;
            int number = userNumber;
            int i;
            for (i = 0; number > 0; i++, number /= 10) {
                digit = number % 10;
                if(i != 0 && nextDigit > digit){
                    validityCounter++;
                }
                nextDigit = digit;

            }
            if((i - 1) == validityCounter) {
                System.out.println("Number with increasing digits = " + userNumber);
                return;
            }
        }
        System.out.println("Number with increasing digits Not Found!!!");
    }

    public static void printNumberWithUnrepeatedDigits(int[] userNumbers){
        Set<Integer> uniqueDigitsSet = new HashSet<>();

        for (int userNumber : userNumbers) {
            int currentNumber = userNumber;
            int currentNumberLength = 0;
            while(currentNumber > 0){
                uniqueDigitsSet.add(currentNumber % 10);
                currentNumber /= 10;
                currentNumberLength++;
            }

            if(currentNumberLength == uniqueDigitsSet.size()) {
                System.out.println("Number with unrepeated digits = " + userNumber);
                return;
            }

            uniqueDigitsSet.clear();
        }
        System.out.println("Number with unrepeated digits Not Found!!!");
    }
}
